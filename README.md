# SHACL sandbox

## Overview

This project aims at providing a simple utility to develop SHACL rules. The result of rules against a specified knowledge base can be checked, in order to validate that the rule behaves as expected.

## Usage

The project is managed through maven. 
* To compile: `mvn compile`
* To run: `mvn exec:java -Dkb=<path/to/kb> -Drules=<path/to/rule/file> -Dout=<path/to/desired/output>`. The software may be called through the provided bash script, which expects the arguments in the order specified in the previous example. The bash script jumps to the current folder before executing the command to enable the usage of relative paths. NB: `ln -s <path/to/target> <name>` creates a link to the specified target. If executed in a folder part of $PATH (i.e. /usr/local/bin), said link can be used to access the utility system-wise.