#! /bin/bash
script=`readlink ${0}`
mvn_path=`dirname $script`
wd=`pwd`
if [ $# -eq 3 ]
then
	cd $mvn_path
	mvn exec:java -Dkb=$wd/$1 -Drules=$wd/$2 -Dout=$wd/$3
else
	echo "usage : $0 <path/to/kb> <path/to/rules> <path/to/result>"
fi
