package fr.irit.ut2j.shacl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.jena.query.QueryParseException;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.RiotException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.topbraid.shacl.rules.RuleUtil;



public class Sandbox {
	
	private static final Logger LOGGER = LogManager.getLogger(Sandbox.class);
	
	/**
	 * 
	 * @param args Arguments are passed via the mvn command line. arg[0] is the kb path, and arg[1] is the rule path
	 */
	public static void main(String[] args) {
		if(args.length < 2) {
			LOGGER.error("usage : mvn exec:java -Dkb=<path/to/kb> -Drules=<path/to/rules> -Dout=<path/to/output>");
		} else {
			String dataPath = args[0];
			String rulesPath= args[1];
			String outputPath = args[2];
			Model data = ModelFactory.createDefaultModel();
			Model rules = ModelFactory.createDefaultModel();
			try (FileInputStream f = new FileInputStream(new File(dataPath))){
				data.read(f, "http://example.org/ns#", "TTL");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (RiotException e) {
				LOGGER.error("Failed to parse data file");
				e.printStackTrace();
				System.exit(-1);
			}
			try (FileInputStream f = new FileInputStream(new File(rulesPath))){
				rules.read(f, "http://example.org/ns#", "TTL");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}catch (RiotException e) {
				LOGGER.error("Failed to parse rule file");
				e.printStackTrace();
				System.exit(-1);
			}
			try {
				Model inferences = RuleUtil.executeRules(data, rules, null, null);
				try(FileOutputStream f = new FileOutputStream(new File(outputPath))){
					inferences.write(f, "TTL");
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} catch (QueryParseException e) {
				LOGGER.error("Failed to apply rules");
				e.printStackTrace();
				System.exit(-1);
			}
			
		}
	}
}
